#include <iostream>
#include <hml/algo/find.hpp>
#include <hml/algo/elem.hpp>
#include <hml/algo/filter.hpp>
#include <hml/algo/dot.hpp>
#include <hml/algo/not.hpp>
#include <hml/algo/range.hpp>
#include <hml/algo/plusplus.hpp>
#include <hml/algo/foldr.hpp>
#include <hml/algo/concat.hpp>
#include <hml/algo/map.hpp>

template<typename x = hml::Placeholder>
struct isList {
    using type = hml::False;
};

template<typename... xs>
struct isList<hml::List<xs...>> {
    using type = hml::True;
};

int main() {
    using TestList = hml::List<hml::List<int>, hml::List<double>, hml::List<float>>;
    static_assert(std::is_same_v<hml::find<hml::elem<float>, TestList>::type, hml::Just<hml::List<float>>>, "");
    static_assert(std::is_same_v<hml::find<hml::elem<short>, TestList>::type, hml::Nothing>, "");

    using TestList1 = hml::List<int, double, hml::List<>>;
    static_assert(std::is_same_v<hml::filter<isList<>, TestList1>::type, hml::List<hml::List<>>>, "");
    static_assert(std::is_same_v<hml::filter<hml::dot<hml::nay<>, isList<>>::type, TestList1>::type, hml::List<int, double>>, "");

    static_assert(std::is_same_v<hml::map<isList<>, hml::List<float, hml::List<>, hml::List<float>, short>>::type, hml::List<hml::False, hml::True, hml::True, hml::False>>);

    static_assert(std::is_same_v<hml::apply1<hml::List<hml::Placeholder>, int>::type, hml::List<int>>, "");
    static_assert(std::is_same_v<hml::apply1<hml::List<hml::List<hml::Placeholder>>, int>::type, hml::List<hml::List<int>>>, "");
    static_assert(std::is_same_v<hml::apply2<hml::List<hml::Placeholder>, int>::type, hml::List<hml::Placeholder>>, "");
    static_assert(std::is_same_v<hml::apply2<hml::List<hml::List<hml::Placeholder>>, int>::type, hml::List<hml::List<hml::Placeholder>>>, "");
    static_assert(std::is_same_v<hml::apply2<hml::List<hml::Placeholder2>, int>::type, hml::List<int>>, "");
    static_assert(std::is_same_v<hml::apply2<hml::List<hml::List<hml::Placeholder2>>, int>::type, hml::List<hml::List<int>>>, "");

    static_assert(std::is_same_v<hml::range<hml::Int<0>, hml::Int<5>>::type, hml::List<hml::Int<0>, hml::Int<1>, hml::Int<2>, hml::Int<3>, hml::Int<4>>>, "");

    static_assert(std::is_same_v<hml::plusplus<hml::List<float, short>, hml::List<char>>::type, hml::List<float, short, char>>);

    static_assert(std::is_same_v<hml::foldr<hml::plusplus<>, hml::List<>, hml::List<hml::List<short, float>, hml::List<char>>>::type, hml::List<short, float, char>>);
    static_assert(std::is_same_v<hml::concat<hml::List<hml::List<short, float>, hml::List<char>>>::type, hml::List<short, float, char>>);

}
