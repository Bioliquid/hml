#pragma once

namespace hml {

template<bool, typename>
struct Maybe {};

using Nothing = Maybe<false, void>;

template<typename a>
using Just = Maybe<true, a>;

}  // namespace hml
