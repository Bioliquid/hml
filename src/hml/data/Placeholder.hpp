#pragma once

namespace hml {

struct Placeholder1 {};
struct Placeholder2 {};

using Placeholder = Placeholder1;

}  // namespace hml
