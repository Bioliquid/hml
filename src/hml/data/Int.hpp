#pragma once

#include <cstdint>
#include <type_traits>

namespace hml {

template<int32_t n>
using Int = std::integral_constant<int32_t, n>;

}  // namespace hml
