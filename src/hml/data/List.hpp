#pragma once

#include "Semigroup.hpp"

namespace hml {

// List :: [a]
template<typename... xs>
struct List {
    using type = List<xs...>;
};

template<typename... xs>
struct Semigroup<List<xs...>> {

};

}  // namespace hml
