#pragma once

#include <type_traits>

namespace hml {

template<bool b>
using Bool = std::bool_constant<b>;

using True = std::true_type;
using False = std::false_type;

}  // namespace hml
