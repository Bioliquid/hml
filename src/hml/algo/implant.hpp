#pragma once

#include "List.hpp"

namespace hml {

// implant :: t -> [a] -> t [a]
template<typename, typename>
struct implant;

template<typename t, typename... xs>
struct implant<t, hml::List<xs...>> {
    using type = t<xs...>;
};

}  // namespace hml
