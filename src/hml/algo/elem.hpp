#pragma once

#include "List.hpp"
#include "Placeholder.hpp"

#include <type_traits>

namespace hml {

// elem :: a -> [a] -> Bool
template<typename, typename = Placeholder>
struct elem;

template<typename x, typename... xs>
struct elem<x, List<xs...>> {
    using type = std::disjunction<std::is_same<x, xs>...>::type;
};

}  // namespace hml
