#pragma once

#include "List.hpp"
#include "Placeholder.hpp"

namespace hml {

// head :: [a] -> a
template<typename = Placeholder>
struct head;

template<typename x, typename... xs>
struct head<List<x, xs...>> {
    using type = x;
};

}  // namespace hml
