#pragma once

#include "foldr.hpp"
#include "plusplus.hpp"

namespace hml {

// concat :: [[a]] -> [a]
template<typename x>
struct concat {
    using type = foldr<plusplus<>, hml::List<>, x>::type;
};

}  // namespace hml
