#pragma once

#include "List.hpp"
#include "cons.hpp"
#include "apply.hpp"

namespace hml {

// map :: (a -> b) -> [a] -> [b]
template<typename, typename = Placeholder>
struct map;

template<typename f>
struct map<f, List<>> {
    using type = List<>;
};

template<typename f, typename x, typename... xs>
struct map<f, List<x, xs...>> {
private:
    using f1 = apply1<f, x>::type;

public:
    using type = typename cons<typename f1::type,
        typename map<f, List<xs...>>::type>::type;
};

}  // namespace hml
