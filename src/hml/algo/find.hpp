#pragma once

#include "List.hpp"
#include "Placeholder.hpp"
#include "Maybe.hpp"
#include "apply.hpp"
#include "conditional.hpp"

#include <type_traits>

namespace hml {

// find :: (a -> Bool) -> [a] -> Maybe a
template<typename, typename = Placeholder>
struct find;

template<typename f>
struct find<f, List<>> {
    using type = Nothing;
};

template<typename f, typename x, typename... xs>
struct find<f, List<x, xs...>> {
private:
    using f1 = apply1<f, x>::type;

public:
    using type = conditional<typename f1::type, Just<x>, typename find<f, List<xs...>>::type>::type;
    // listToMaybe . filter p
};

}  // namespace hml
