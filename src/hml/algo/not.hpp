#pragma once

#include "Bool.hpp"
#include "Placeholder.hpp"

namespace hml {

// not :: Bool -> Bool
template<typename = Placeholder>
struct nay;

template<>
struct nay<True> {
    using type = False;
};

template<>
struct nay<False> {
    using type = True;
};


}  // namespace hml
