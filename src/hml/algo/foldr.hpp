#pragma once

#include "List.hpp"

namespace hml {

// foldr :: (a -> b -> b) -> b -> [a] -> b
template<typename, typename, typename>
struct foldr;

template<typename k, typename z, typename xs>
struct foldr {
private:
    template<typename>
    struct go;

    template<>
    struct go<hml::List<>> {
        using type = z;
    };

    template<typename y, typename... ys>
    struct go<hml::List<y, ys...>> {
    private:
        using f1 = apply1<k, typename go<hml::List<ys...>>::type>::type;
        using f2 = apply2<f1, y>::type;

    public:
        using type = typename f2::type;
    };

public:
    using type = go<xs>::type;
};

}  // namespace hml
