#pragma once

#include "List.hpp"
#include "Placeholder.hpp"

namespace hml {

// tail :: [a] -> [a]
template<typename = Placeholder>
struct tail;

template<typename x, typename... xs>
struct tail<List<x, xs...>> {
    using type = List<xs...>;
};

}  // namespace hml
