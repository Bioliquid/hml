#pragma once

#include "Bool.hpp"

namespace hml {

template<typename, typename, typename>
struct conditional;

template<typename a, typename b>
struct conditional<True, a, b> {
    using type = a;
};

template<typename a, typename b>
struct conditional<False, a, b> {
    using type = b;
};
}  // namespace hml
