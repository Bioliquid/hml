#pragma once

#include "List.hpp"

namespace hml {

// length :: [a] -> Int
template<typename>
struct length;

template<typename... xs>
struct length<List<xs...>> {
    static constexpr size_t value = sizeof...(xs);
};

}  // namespace hml
