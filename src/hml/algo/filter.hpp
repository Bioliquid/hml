#pragma once

#include "List.hpp"
#include "Placeholder.hpp"
#include "apply.hpp"
#include "cons.hpp"
#include "conditional.hpp"

#include <type_traits>

namespace hml {

// filter :: (a -> Bool) -> [a] -> [a]
template<typename, typename = Placeholder>
struct filter;

template<typename f>
struct filter<f, List<>> {
    using type = List<>;
};

template<typename f, typename x, typename... xs>
struct filter<f, List<x, xs...>> {
private:
    using f1 = apply1<f, x>::type;

public:
    using type = conditional<typename f1::type,
        typename cons<x, typename filter<f, List<xs...>>::type>::type,
        typename filter<f, List<xs...>>::type>::type;
};

}  // namespace hml
