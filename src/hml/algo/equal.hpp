#pragma once

#include "Placeholder.hpp"

#include <type_traits>

namespace hml {

// equal :: Eq a => a -> a -> Bool
template<typename a1, typename a2 = Placeholder>
struct equal {
    using type = std::is_same<a1, a2>::type;
};

}  // namespace hml
