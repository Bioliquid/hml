#pragma once

#include "List.hpp"
#include "Int.hpp"
#include "Placeholder.hpp"

#include <tuple>

namespace hml {

// at :: Int -> [a] -> a
template<typename, typename = Placeholder>
struct at;

template<int32_t n, typename... xs>
struct at<Int<n>, List<xs...>> {
    using type = std::tuple_element<n, std::tuple<xs...>>::type;
};

}  // namespace hml
