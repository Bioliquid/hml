#pragma once

#include "List.hpp"

namespace hml {

// (++) :: [a] -> [a] -> [a]
template<typename = Placeholder2, typename = Placeholder>
struct plusplus;

template<typename... xs, typename... ys>
struct plusplus<hml::List<xs...>, hml::List<ys...>> {
    using type = hml::List<xs..., ys...>;
};

}  // namespace hml
