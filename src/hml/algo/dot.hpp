#pragma once

#include "apply.hpp"

namespace hml {

// (.) :: (b -> c) -> (a -> b) -> a -> c
template<typename f1, typename f2>
struct dot {
    using type = apply1<f1, f2>::type;
};

}  // namespace hml
