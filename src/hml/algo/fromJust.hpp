#pragma once

#include "Maybe.hpp"

namespace hml {

// fromJust :: Maybe a -> a
template<typename>
struct fromJust;

template<typename T>
struct fromJust<hml::Just<T>> {
    using type = T;
};

}  // namespace hml
