#pragma once

#include "List.hpp"
#include "Placeholder.hpp"

namespace hml {

// cons :: a -> [a] -> [a]
template<typename, typename = Placeholder>
struct cons;

template<typename x, typename... xs>
struct cons<x, List<xs...>> {
    using type = List<x, xs...>;
};

}  // namespace hml
