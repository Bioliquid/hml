#pragma once

#include "Int.hpp"
#include "List.hpp"

namespace hml {

// range :: Int -> Int -> [Int]
template<typename, typename>
struct range;

template<int32_t b, int32_t e> requires(b <= e)
struct range<Int<b>, Int<e>> {
    using type = hml::cons<hml::Int<b>, typename range<Int<b + 1>, Int<e>>::type>::type;
};

template<int32_t b>
struct range<Int<b>, Int<b>> {
    using type = hml::List<>;
};


}  // namespace hml
