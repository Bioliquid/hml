#pragma once

#include "Placeholder.hpp"

namespace hml {

template<typename p, typename a, typename x>
struct substitute {
    using type = x;
};

template<typename p, typename a>
struct substitute<p, a, p> {
    using type = a;
};

template<typename p, typename a, template<typename...> typename f, typename... xs>
struct substitute<p, a, f<xs...>> {
    using type = f<typename substitute<p, a, xs>::type...>::type;
};

template<typename, typename, typename>
struct apply;

template<typename p, template<typename...> typename f, typename... xs, typename a>
struct apply<p, f<xs...>, a> {
    using type = f<typename substitute<p, a, xs>::type...>;
};

template<typename x, typename a>
using apply1 = apply<Placeholder1, x, a>;

template<typename x, typename a>
using apply2 = apply<Placeholder2, x, a>;

}  // namespace hml
